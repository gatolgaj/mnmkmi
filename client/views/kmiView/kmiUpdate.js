/**
 * Created by shyam on 12/02/17.
 */

Template.kmiUpdateForm.rendered = function(){
    var kmiId = FlowRouter.getParam("kmiId");
    var scodeData = MNM.Collections.Kmi.findOne(kmiId).getScoreOfEachGroup().kmiGroupsResult;
    Session.set("kmiTeamsize", MNM.Collections.Kmi.findOne(kmiId).teamSize);
    Session.set("kmiQuarter", MNM.Collections.Kmi.findOne(kmiId).Quarter);


    var barData = {
        labels: scodeData.map(function(obj) {return obj.paramGrpName}),
        datasets: [
            {
                label: "Data 1",
                backgroundColor: 'rgba(220, 220, 220, 0.5)',
                pointBorderColor: "#fff",
                data: scodeData.map(function(obj) {return obj.paramGrpScore})
            }
        ]
    };

    var barOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    max: 10,
                    min: 0,
                    beginAtZero: true
                }
            }]
        }
    };


    var ctx2 = document.getElementById("barChart").getContext("2d");
    new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
}
Template.kmiUpdateForm.helpers({
    curdoc :  function () {
        var kmiId = FlowRouter.getParam("kmiId");

        console.log( kmiId,MNM.Collections.Kmi.findOne(kmiId));

        return MNM.Collections.Kmi.findOne(kmiId);
    },
    getScore : function() {
       var kmiId = FlowRouter.getParam("kmiId");
        return MNM.Collections.Kmi.findOne(kmiId).getScoreOfEachGroup().totalScore;
        /*    var barData = {
            labels: scodeData.map(function(obj) {return obj.paramGrpName}),
            datasets: [
                {
                    label: "Data 1",
                    backgroundColor: 'rgba(220, 220, 220, 0.5)',
                    pointBorderColor: "#fff",
                    data: scodeData.map(function(obj) {return obj.paramGrpScore})
                }
            ]
        };

        var barOptions = {
            responsive: true
        };


        var ctx2 = document.getElementById("barChart").getContext("2d");
        new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});*/
    }
})


AutoForm.hooks({
    kmiUpdateFormId: {
/*        onSubmit: function (ins, update) {
            //console.log(this)
            console.log(ins)
            //console.log(update)
            //CommunitySchema.clean(update)
            console.log(update)

            ActivitySchema.clean(ins)
            //Activity.update({_id: this.docId}, update)
            this.done()

            //this.event.preventDefault()
            // return false;
        },*/
        onSuccess: function (formType, result) {

            toastr.success('KMI System', 'KMI Saved Successfully', {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });

        },
        onError: function (formType, error) {
            // //console.log(error);
            toastr.error('KMI System', 'Saving KMI info failed' + error, {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        },
/*        before:{
            insert:function(ins){

                return ins
            },
            update:function(doc){
                // ActivitySchema.clean(doc)
                console.log(doc)
                var obj = ActivityScratch.update({_id:Template.instance().parent().activityId},doc)
            }
        }*/
    },

});