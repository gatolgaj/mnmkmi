/**
 * Created by shyam on 14/03/17.
 */
Template.kmiInsertForm.helpers({
    insertDoc: function () {

        var retobj = Session.get("retObj")
      //  AutoForm.getFieldValue("Quarter", "kmiInsertFormId");
        MNM.Schemas.KmiSchema.clean(retobj)
        return retobj;
    },
    teamSizeValue: function () {
        return Session.get("retObj").KMIInputsInfo.teamSize;
    }
})

Template.kmiInsertForm.events({
    'click #fetch': function (event) {
        event.preventDefault();
        var division = AutoForm.getFieldValue("division", "kmiInsertFormId");
        var coe = AutoForm.getFieldValue("coe", "kmiInsertFormId");
        Session.set("kmiQuarter",AutoForm.getFieldValue("Quarter", "kmiInsertFormId"));

        console.log(AutoForm)
        console.log(division, coe);
        if (MNM.Collections.KmiCOEInfo.find({division: division, coe: coe}).fetch().length > 0) {
            var coeFound = MNM.Collections.KmiCOEInfo.find({division: division, coe: coe}).fetch()[0];
           console.log(coeFound.teamSize);
            Session.set("kmiTeamsize",coeFound.teamSize);
            var retObj = new Object();
            retObj.KMIInputsInfo = coeFound;
            retObj.coe = coe;
            retObj.division= division;
            retObj.teamSize = coeFound.teamSize;
            MNM.Schemas.KmiSchema.clean(retObj);


            console.log(retObj);
            Session.set("retObj", retObj);
            $("#groupInfo").show()
        } else {
            toastr.error('KMI System', 'No Setup Found for Division : '+division+' and COE : ' + coe, {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        }
    }
})

AutoForm.hooks({
    kmiInsertFormId: {
        /*        onSubmit: function (ins, update) {
         //console.log(this)
         console.log(ins)
         //console.log(update)
         //CommunitySchema.clean(update)
         console.log(update)

         ActivitySchema.clean(ins)
         //Activity.update({_id: this.docId}, update)
         this.done()

         //this.event.preventDefault()
         // return false;
         },*/
        onSuccess: function (formType, result) {

            toastr.success('KMI System', 'KMI  Saved Successfully', {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });

        },
        onError: function (formType, error) {
            // //console.log(error);
            toastr.error('KMI System', 'Saving KMI  info failed' + error, {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        },
        /*        before:{
         insert:function(ins){

         return ins
         },
         update:function(doc){
         // ActivitySchema.clean(doc)
         console.log(doc)
         var obj = ActivityScratch.update({_id:Template.instance().parent().activityId},doc)
         }
         }*/
    },

});