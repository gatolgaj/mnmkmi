/**
 * Created by shyam on 12/02/17.
 */

Template.kmiParameterList.rendered = function(){

    // Set white background color for top navbar
    $('body').addClass('light-navbar');

    var sparklineCharts = function(){
        $("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 52], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#1ab394',
            fillColor: "transparent"
        });

        $("#sparkline2").sparkline([32, 11, 25, 37, 41, 32, 34, 42], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#1ab394',
            fillColor: "transparent"
        });

        $("#sparkline3").sparkline([34, 22, 24, 41, 10, 18, 16,8], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#1C84C6',
            fillColor: "transparent"
        });
    };

    var sparkResize;

    $(window).resize(function(e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparklineCharts, 500);
    });

    sparklineCharts();




    var data1 = [
        [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,20],[11,10],[12,13],[13,4],[14,7],[15,8],[16,12]
    ];
    var data2 = [
        [0,0],[1,2],[2,7],[3,4],[4,11],[5,4],[6,2],[7,5],[8,11],[9,5],[10,4],[11,1],[12,5],[13,2],[14,5],[15,2],[16,0]
    ];
    $("#flot-dashboard5-chart").length && $.plot($("#flot-dashboard5-chart"), [
            data1,  data2
        ],
        {
            series: {
                lines: {
                    show: false,
                    fill: true
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
                points: {
                    radius: 0,
                    show: true
                },
                shadowSize: 2
            },
            grid: {
                hoverable: true,
                clickable: true,

                borderWidth: 2,
                color: 'transparent'
            },
            colors: ["#1ab394", "#1C84C6"],
            xaxis:{
            },
            yaxis: {
            },
            tooltip: false
        }
    );
};

Template.kmiParameterList.helpers({

/*    communityData: function () {
        // //console.log( BSMHQ.Collections.Media.find().fetch());

        var orgData = Community.find({}).fetch().map(function(org,index) {
            var retObj = org.raw();
            retObj.srNo = index+1;
            return retObj;
        });
        return orgData;
    },*/
    myCollection: function () {
        return MNM.Collections.KmiGroups;
    }
,
   kmiFields: function () {
        return [
            {fieldId: 'parameterGroupName', key: 'parameterGroupName', label: 'Group Name', sort: 'ascending'},
            
            {
                fieldId: 'editOrg',
                key: 'editOrg ', label: 'Action',
                fn: function (value, object) {
                    return new Spacebars.SafeString('<div> <a href="#" class="btn btn-primary btn-xs editOrg">View</a> </div>'
                    );
                },
                sortable: false, headerClass: 'col-md-2'
            }

        ];
    },
    /*
    communitySettings: function () {
        return {
            rowsPerPage: 4,
            showFilter: true
        };
    },*/

})


Template.kmiParameterList.events({
    'click .reactive-table tbody tr': function (event) {
        event.preventDefault();
        // var lineItem = this;
        // checks if the actual clicked element has the class `delete`
        // //console.log(event.target.className);
        if (event.target.className == "btn btn-primary btn-xs editOrg") {

            FlowRouter.go('/viewKmiParam/'+this._id);
            // LineItems.update(this._id, {$set: {'status': 'Deleted'}})
        }

    }
})