/**
 * Created by shyam on 12/02/17.
 */
Template.kmiCOEUpdateForm.helpers({
    curdoc :  function () {
        var kmiId = FlowRouter.getParam("kmiId");

        console.log( kmiId,MNM.Collections.KmiCOEInfo.findOne(kmiId))
        return MNM.Collections.KmiCOEInfo.findOne(kmiId);
    }
})

Template.kmiCOEUpdateForm.rendered = function () {
    // console.log(document.getElementsByClassName("checkbox"));
    var elements =  document.getElementsByClassName("checkbox");
    while (elements.length) elements[0].classList.remove('checkbox')

    Session.clear("kmiTeamsize");
    Session.clear("kmiQuarter");
}

AutoForm.hooks({
    kmiCOEFormId: {
        /*        onSubmit: function (ins, update) {
         //console.log(this)
         console.log(ins)
         //console.log(update)
         //CommunitySchema.clean(update)
         console.log(update)

         ActivitySchema.clean(ins)
         //Activity.update({_id: this.docId}, update)
         this.done()

         //this.event.preventDefault()
         // return false;
         },*/
        onSuccess: function (formType, result) {

            toastr.success('KMI System', 'KMI  Saved Successfully', {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });

        },
        onError: function (formType, error) {
            // //console.log(error);
            toastr.error('KMI System', 'Saving KMI  info failed' + error, {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        },
        /*        before:{
         insert:function(ins){

         return ins
         },
         update:function(doc){
         // ActivitySchema.clean(doc)
         console.log(doc)
         var obj = ActivityScratch.update({_id:Template.instance().parent().activityId},doc)
         }
         }*/
    },

});