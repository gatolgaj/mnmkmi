Template.dashboard2.rendered = function(){

    // Set white background color for top navbar
    // Initialize select2

    $(".select2_demo_1").select2();
    $(".select2_demo_2").select2();
    $(".select2_demo_3").select2({
        placeholder: "Select a COE",
        allowClear: true
    });

    $(".select2_demo_4").select2({
        placeholder: "Select a Year",
        allowClear: true
    });
    $('body').addClass('light-navbar');

    // Options, data for charts
    var data2 = [
        [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
        [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
        [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
        [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
        [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
        [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
        [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
        [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
    ];

    var data3 = [
        [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
        [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
        [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
        [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
        [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
        [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
        [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
        [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
    ];

    var dataset = [
        {
            label: "Number of orders",
            data: data3,
            color: "#1ab394",
            bars: {
                show: true,
                align: "center",
                barWidth: 24 * 60 * 60 * 600,
                lineWidth:0
            }

        }, {
            label: "Payments",
            data: data2,
            yaxis: 2,
            color: "#1C84C6",
            lines: {
                lineWidth:1,
                show: true,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            },
            splines: {
                show: false,
                tension: 0.6,
                lineWidth: 1,
                fill: 0.1
            }
        }
    ];


    var options = {
        xaxis: {
            mode: "time",
            tickSize: [3, "day"],
            tickLength: 0,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 10,
            color: "#d5d5d5"
        },
        yaxes: [{
            position: "left",
            max: 1070,
            color: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 3
        }, {
            position: "right",
            clolor: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: ' Arial',
            axisLabelPadding: 67
        }
        ],
        legend: {
            noColumns: 1,
            labelBoxBorderColor: "#000000",
            position: "nw"
        },
        grid: {
            hoverable: false,
            borderWidth: 0
        }
    };

    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
    }


    $.plot($(".flot-dashboard-chart"), dataset, options);

    // Options, data for map plugin

    var mapData = {
        "US": 298,
        "SA": 200,
        "DE": 220,
        "FR": 540,
        "CN": 120,
        "AU": 760,
        "BR": 550,
        "IN": 200,
        "GB": 120
    };

    $('#world-map').vectorMap({
        map: 'world_mill_en',
        backgroundColor: "transparent",
        regionStyle: {
            initial: {
                fill: '#e4e4e4',
                "fill-opacity": 0.9,
                stroke: 'none',
                "stroke-width": 0,
                "stroke-opacity": 0
            }
        },

        series: {
            regions: [{
                values: mapData,
                scale: ["#1ab394", "#22d6b1"],
                normalizeFunction: 'polynomial'
            }]
        }
    });
};

Template.dashboard2.destroyed = function(){
    // Remove special class
    $('body').removeClass('light-navbar');
};

Template.dashboard2.helpers({
    coeList :  function () {
        return MNM.Collections.adminValueSet.find({type:"COE",parentValue:Session.get("dashBoardDivision")});

    },
    divisionList :  function () {
        return MNM.Collections.adminValueSet.find({type:"Division"});
    },
    periodList :  function () {
        return MNM.Collections.adminValueSet.find({type:"Period"});
    },
    getKmi  :   function() {
      //  console.log("getIN")
        var dDiv=Session.get("dashBoardDivision");
        var dCoe=Session.get("dashBoardCOE");
        var dPeriod=Session.get("dashBoardPeriod");
        return MNM.Collections.Kmi.find({division:dDiv,coe:dCoe,period:dPeriod}).fetch();
    }
})


Template.dashboard2.events({
    'click #viewResult': function(event,tpl) {
    // Prevent default browser form submit
        console.log("This")
        Session.set("dashBoardDivision", tpl.$('[name=dDivision]').val())
        Session.set("dashBoardCOE",tpl.$('[name=dCOE]').val())
        Session.set("dashBoardPeriod",tpl.$('[name=dPeriod]').val())
},
    'change #dDivision': function(event,tpl) {
        // Prevent default browser form submit
        console.log("This")
        Session.set("dashBoardDivision", tpl.$('[name=dDivision]').val())
      //  Session.set("dashBoardCOE",tpl.$('[name=dCOE]').val())
    },
});

Template.chartTemplate.rendered = function() {
    var dynamicColors = function (i) {
        var default_colors = ['#3366CC','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477','#66AA00','#B82E2E','#316395','#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707','#329262','#5574A6','#3B3EAC']
        return default_colors[i];
    };

    console.log(Template.currentData())
   var kmiId = Template.currentData()._id;

    var scodeData = MNM.Collections.Kmi.findOne(kmiId).getScoreOfEachGroup().kmiGroupsResult;
    var labels = scodeData.map(function(obj) {return obj.paramGrpName});
    labels.push("KMI Score");
    var graphData = scodeData.map(function(obj) {return obj.paramGrpScore});
    graphData.push(MNM.Collections.Kmi.findOne(kmiId).getScoreOfEachGroup().totalScore);
    var pointBackgroundColors = [];
    var barData = {
        labels:labels ,
        datasets: [
            {
                label: "Total KMI Score",
                backgroundColor: pointBackgroundColors,
                fillColor: pointBackgroundColors,
                pointBorderColor: pointBackgroundColors,
                pointBackgroundColor: pointBackgroundColors,

                data: graphData
            }
        ]
    };

    var barOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    max: 10,
                    min: 0,
                    beginAtZero: true
                }
            }],
            xAxes: [{
                barPercentage: 0.3
            }]
        }
    };


    var ctx2 = this.$(".barChart")[0].getContext("2d");
    var myChart = new Chart(ctx2, {type: 'bar', data: barData, options: barOptions});
    for (var i = 0; i < myChart.data.datasets[0].data.length; i++) {
       pointBackgroundColors.push(dynamicColors(i));
    }

    myChart.update();
}


Template.chartTemplate.helpers({

    getScore : function() {
        var kmiId = Template.currentData()._id;
        return MNM.Collections.Kmi.findOne(kmiId).getScoreOfEachGroup().totalScore.toFixed(2);
    },
    getScoreData : function() {
        var kmiId = Template.currentData()._id;
        var scoreData =  MNM.Collections.Kmi.findOne(kmiId).getScoreOfEachGroup().kmiGroupsResult;
        console.log(scoreData);
        return scoreData.map(function(obj) {return {grpName:obj.paramGrpName,grpScore:obj.paramGrpScore.toFixed(2)}})
    }
})


