/**
 * Created by shyam on 16/03/17.
 */
Template.kmiCOESetupInsertForm.rendered = function () {
   // console.log(document.getElementsByClassName("checkbox"));
    var elements =  document.getElementsByClassName("checkbox");
    while (elements.length) elements[0].classList.remove('checkbox')

    Session.clear("kmiTeamsize");
    Session.clear("kmiQuarter");
}


AutoForm.hooks({
    kmiCOEInsertFormId: {
        onSubmit: function (ins, update) {
         //console.log(this)
         console.log(ins)
         //console.log(update)
         //CommunitySchema.clean(update)
         console.log(update)
         if(MNM.Collections.KmiCOEInfo.find({division:ins.division,coe:ins.coe}).fetch().length > 0) {
             this.done(new Error("Setup Info already Exists"))
         }else
         {
             MNM.Collections.KmiCOEInfo.insert(ins);
             this.done()
            this.event.preventDefault()
         }
          return false;
         },
        onSuccess: function (formType, result) {

            toastr.success('KMI System', 'KMI COE  Saved Successfully', {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });

        },
        onError: function (formType, error) {
            // //console.log(error);
            toastr.error('KMI System', 'Saving KMI COE info failed' + error, {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        },
        /*        before:{
         insert:function(ins){

         return ins
         },
         update:function(doc){
         // ActivitySchema.clean(doc)
         console.log(doc)
         var obj = ActivityScratch.update({_id:Template.instance().parent().activityId},doc)
         }
         }*/
    },

});