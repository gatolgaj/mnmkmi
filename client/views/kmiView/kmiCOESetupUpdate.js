/**
 * Created by shyam on 12/02/17.
 */
Template.kmiCOESetupUpdateForm.helpers({
    curdoc :  function () {
        var kmiId = FlowRouter.getParam("kmiId");

       // console.log( kmiId,MNM.Collections.KmiCOEInfo.findOne(kmiId));
        var kmiCoeInfo =  MNM.Collections.KmiCOEInfo.findOne(kmiId);
        var groups = MNM.Collections.KmiGroups.find({_id: {$in : kmiCoeInfo.KMIGroupsForCOE}}).fetch();
        console.log(kmiCoeInfo.KMIGroupsInfo,groups);
        if(typeof (kmiCoeInfo.KMIGroupsInfo) == "undefined")
            kmiCoeInfo.KMIGroupsInfo = [].concat(groups);
        else{
           // var temp =  [].concat(kmiCoeInfo.KMIGroupsInfo);
            var updatedGrp = [];
            _.each(kmiCoeInfo.KMIGroupsInfo,function(group) {
                _.each(groups,function(grp) {
                    if(grp.parameterGroupName==group.parameterGroupName) {
                        updatedGrp.push(group);
                    }
                    var newGrp = kmiCoeInfo.KMIGroupsInfo.find(function(g2){
                        if(g2.parameterGroupName==this.parameterGroupName) {
                            return true
                        }

                    },grp)

                    console.log("grp 1",grp);
                    console.log("Grp 2",newGrp);
                    if((typeof newGrp =="undefined") && !_.contains(updatedGrp,grp)) {
                        updatedGrp.push(grp);
                    }
                })
            })
            kmiCoeInfo.KMIGroupsInfo = [].concat(updatedGrp);

        }

        console.log(kmiCoeInfo);
        return kmiCoeInfo;
    }
})

Template.kmiCOESetupUpdateForm.rendered = function () {
    // console.log(document.getElementsByClassName("checkbox"));
    var elements =  document.getElementsByClassName("checkbox");
    while (elements.length) elements[0].classList.remove('checkbox')

    Session.clear("kmiTeamsize");
    Session.clear("kmiQuarter");

   // Session.
}


AutoForm.hooks({
    kmiCOESetupFormId: {
        /*        onSubmit: function (ins, update) {
         //console.log(this)
         console.log(ins)
         //console.log(update)
         //CommunitySchema.clean(update)
         console.log(update)

         ActivitySchema.clean(ins)
         //Activity.update({_id: this.docId}, update)
         this.done()

         //this.event.preventDefault()
         // return false;
         },*/
        onSuccess: function (formType, result) {

            toastr.success('KMI System', 'KMI  Saved Successfully', {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });

        },
        onError: function (formType, error) {
            // //console.log(error);
            toastr.error('KMI System', 'Saving KMI  info failed' + error, {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        },
        /*        before:{
         insert:function(ins){

         return ins
         },
         update:function(doc){
         // ActivitySchema.clean(doc)
         console.log(doc)
         var obj = ActivityScratch.update({_id:Template.instance().parent().activityId},doc)
         }
         }*/
    },

});