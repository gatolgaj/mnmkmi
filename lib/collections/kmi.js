/**
 * Created by shyam on 12/02/17.
 */
MNM = {};
MNM.Collections = {};
MNM.Schemas = {};
MNM.Collections.Kmi = new Meteor.Collection('kmi');
MNM.Collections.adminValueSet = new Meteor.Collection('adminValueSet');
MNM.Collections.Kmi.allow({
    update: function (userId, doc, fieldNames, modifier) {
        //  if (Meteor.users.findOne({_id: userId}).username === doc.userName)
        return true;
    },
    insert: function (userId, doc) {
        // only allow posting if you are logged in
        return true;
        //return !!userId;
    }
});
MNM.Collections.KmiGroups = new Meteor.Collection('kmiGroup');
MNM.Collections.KmiCOEInfo = new Meteor.Collection('kmiCoeInfo');
filesStore = new FS.Store.GridFS("filesStore", {});

FilesStoreCollection = new FS.Collection("filesStore", {
    stores: [filesStore]
});


FilesStoreCollection.allow({
    update: function (userId, doc, fieldNames, modifier) {
        //  if (Meteor.users.findOne({_id: userId}).username === doc.userName)
        return true;
    },
    insert: function (userId, doc) {
        // only allow posting if you are logged in
        return true;
        //return !!userId;
    },
    download: function (userId, doc) {
        // only allow posting if you are logged in
        return true;
        //return !!userId;
    },
})
MNM.Collections.KmiGroups.allow({
    update: function (userId, doc, fieldNames, modifier) {
        //  if (Meteor.users.findOne({_id: userId}).username === doc.userName)
        return true;
    },
    insert: function (userId, doc) {
        // only allow posting if you are logged in
        return true;
        //return !!userId;
    }
});

MNM.Collections.KmiCOEInfo.allow({
    update: function (userId, doc, fieldNames, modifier) {
        //  if (Meteor.users.findOne({_id: userId}).username === doc.userName)
        return true;
    },
    insert: function (userId, doc) {
        // only allow posting if you are logged in
        return true;
        //return !!userId;
    }
});
MNM.Schemas.adminValueSet = new SimpleSchema({
    type: {
        type: String,
        label: 'Type',
        allowedValues: [
            'Division', 'COE', 'Period', 'Quarter' // , 'Brand Ambassadors', 'Hyperlocal',Internet-Smartphone'
        ],
        autoform: {
            afFieldInput: {
                type: 'select',
                options: 'allowed'
            }
        }
    },
    value: {
        type: String,
        label: 'Value'
    },
    parentValue: {
        type: String,
        label: 'Parent Value',
        defaultValue: "NONE"
    }


})
MNM.Schemas.scoreRange = new SimpleSchema({
    startRange: {
        type: Number,
        label: 'Start Range',
        decimal: true
    },
    endRange: {
        type: Number,
        label: 'End Range',
        decimal: true,
        defaultValue : 100
    },
    score: {
        type: Number,
        label: 'Score'
    },

})

MNM.Schemas.expectedQuantityEval = new SimpleSchema({
    halfYearly: {
        type: Number,
        label: 'Half Year',
        decimal: true
    },
    quarterly: {
        type: Number,
        label: 'Quarter',
        decimal: true
    },
    fullYear: {
        type: Number,
        label: 'Full Year',
        decimal: true
    },

})

MNM.Schemas.expectedAndActual = new SimpleSchema({
    expected: {
        type: Number,
        label: 'Expected Quantity',
        defaultValue :0,
        autoform: {
            readOnly : true
        }
    },
    actual: {
        type: Number,
        label: 'Actual Quantity',
        defaultValue :0

    },


})
MNM.Schemas.KmiParameterMeasure = new SimpleSchema({
    typeOfMeasure: {
        type: String,
        label: 'Type Of Measure',

        allowedValues: [
            'Quantitative', 'Qualitative' // , 'Brand Ambassadors', 'Hyperlocal',Internet-Smartphone'
        ],
        autoform: {
            afFieldInput: {
                type: 'select',
                options: 'allowed'
            }
        }

    },
    measureDescription: {
        type: String,
        label: 'Measure Description',
    },
    measureWeightage: {
        type: Number,
        label: 'Measure Weightage',
    },
    minimumExpectedQty: {
        type: Number,
        label: 'Minimum Expected Quantity',
        optional: true,
        decimal: true,
        autoValue: function () {
           if(Meteor.isClient) {
               /*console.log(this.siblingField('expectedQuantityEval').value);
                console.log(Session.get("kmiQuarter"));
                console.log(Session.get("kmiTeamsize"));*/
               console.log(this.value)
               if (this.value) return this.value;
               if (!this.siblingField('expectedQuantityEval').isSet) return 0;
               var quarter = Session.get("kmiQuarter");
               var teamSize = Session.get("kmiTeamsize")
               var factor = 1;
               if (quarter == "Q1" || quarter == "Q2" || quarter == "Q3" || quarter == "Q4") {
                   factor = this.siblingField('expectedQuantityEval').value.quarterly;
               } else if (quarter == "H1" || quarter == "H2") {
                   factor = this.siblingField('expectedQuantityEval').value.halfYearly;
               } else {
                   factor = this.siblingField('expectedQuantityEval').value.fullYear;
               }
               var retObj = new Object();
               // retObj.actual=0;
               console.log(factor);
               if(typeof teamSize ==="undefined") return 0;

               retObj.expected = (teamSize/factor)


               return retObj.expected;
           }
        }

    },

    actualQty: {
        type: Number,
        label: 'Actual Quantity',
        optional: true
    },
    typeOfScore: {
        type: String,
        label: 'Scoring Type',

        allowedValues: [
            'Ratio', 'Difference' // , 'Brand Ambassadors', 'Hyperlocal',Internet-Smartphone'
        ],
        autoform: {
            afFieldInput: {
                type: 'select',
                options: 'allowed'
            }
        }

    },
    expectedAndActual: {
        type: MNM.Schemas.expectedAndActual,
        label: 'Expected and Actual',
        optional: true,
/*        autoValue: function () {
            console.log(this.siblingField('expectedQuantityEval').value);
            console.log(this.field('Quarter').value);
            console.log(this.field('teamSize').value);
            var factor = 0;
            if(this.field('Quarter').value=="Q1" || this.field('Quarter').value=="Q3" || this.field('Quarter').value=="Q3" || this.field('Quarter').value=="Q4") {
                    factor = this.siblingField('expectedQuantityEval').value.quarterly;
            } else if(this.field('Quarter').value=="H1" ||this.field('Quarter').value=="H2") {
                factor = this.siblingField('expectedQuantityEval').value.halfYearly;
            } else {
                factor = this.siblingField('expectedQuantityEval').value.fullYear;
            }
            var retObj = new Object();
            retObj.actual=0;
            retObj.expected=factor*this.field('teamSize').value


            return retObj;
        }*/

    },
    index: {
        type: Number,
        label: 'Index',
        optional: true
    },
    scoringCriteria: {
        type: [MNM.Schemas.scoreRange],
        label: 'Scoring Criteria',
        optional: true


    },
    expectedQuantityEval: {
        type: MNM.Schemas.expectedQuantityEval,
        label: 'Expected Quantity Factor',
        optional: true


    },
    score: {
        type: String,
        label: 'Score',
        optional: true
    },

});


MNM.Schemas.KmiParameter = new SimpleSchema({
    parameterName: {
        type: String,
        label: 'KMI Parameter'
    },
    measure: {
        type: [MNM.Schemas.KmiParameterMeasure],
        label: 'Measure',


    },
    kmiParameterScore: {
        type: Number,
        label: 'KMI Parameter Score',
        optional: true
    },
    parameterWeightage: {
        type: Number,
        label: "Parameter Weightage",
        optional: true
    },
    fileId: {
        type: String,
        optional: true,
        label: 'Document',
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'FilesStoreCollection',
                label: "Choose file"
            }
        }
    }
});


MNM.Schemas.KmiParameterGroup = new SimpleSchema({
    parameterGroupName: {
        type: String,
        label: 'KMI Group'
    },
    groupParameters: {
        type: [MNM.Schemas.KmiParameter],
        label: "Parameter"
    },
    groupWeightage: {
        type: Number,
        label: "Group Weightage",
        optional: true
    },

});


MNM.Schemas.KmiCoeSetupSchema = new SimpleSchema({
    division: {
        type: String,
        label: 'Division',

        // allowedValues:  MNM.Collections.adminValueSet.find({type:"Division"}).map(function(obj) {return obj.value}),
        autoform: {
            afFieldInput: {
                type: 'select',
                options: function () {
                    return MNM.Collections.adminValueSet.find({type: "Division"}).map(function (obj) {
                        return {label: obj.value, value: obj.value}
                    })
                }
            }
        }

    },

    coe: {
        type: String,
        label: 'Center Of Excellence',
        // max: 5,
        // allowedValues: MNM.Collections.adminValueSet.find({type:"COE"}).map(function(obj) {return obj.value}),
        autoform: {
            afFieldInput: {
                type: 'select',
                //multiple: true,
                options: function () {
                    var division = AutoForm.getFieldValue('division');
                    return MNM.Collections.adminValueSet.find({type: "COE", parentValue: division}).map(function (obj) {
                        return {label: obj.value, value: obj.value}
                    })
                }
            }
        }
    },
    teamSize: {
        type: Number,
        label: 'Team Size',

        optional: false
    },

    KMIGroupsForCOE: {
        type: [String],
        optional: true,
        label: "KMI Parameters for COE",

        autoform: {
            type: "select-checkbox",
            class: "i-checks",
            options: function () {

                return MNM.Collections.KmiGroups.find().fetch().map(function (group) {
                    return {label: group.parameterGroupName, value: group._id}
                })
                /* return [
                 {label: "2013", value: 2013},
                 {label: "2014", value: 2014},
                 {label: "2015", value: 2015}
                 ];*/
            }
        }
    },
    KMIGroupsInfo: {
        type: [MNM.Schemas.KmiParameterGroup],
        label: "Parameter Group",
        optional: true,
        /*        autoValue: function() {
         var groups = AutoForm.getFieldValue('KMIGroupsForCOE');
         console.log(groups);
         var groupId = client.map(function(group) {
         return group._id;
         })
         return MNM.Collections.KmiGroups.find({_id: {$in : groupId}}).fetch();
         }*/
    },

});

MNM.Schemas.KmiCoeInputSchema = new SimpleSchema({
    KMIGroupsInfo: {
        type: [MNM.Schemas.KmiParameterGroup],
        label: "Parameter Group",
        optional: true,
        /*        autoValue: function() {
         var groups = AutoForm.getFieldValue('KMIGroupsForCOE');
         console.log(groups);
         var groupId = client.map(function(group) {
         return group._id;
         })
         return MNM.Collections.KmiGroups.find({_id: {$in : groupId}}).fetch();
         }*/
    },

});


MNM.Schemas.KmiSchema = new SimpleSchema({
    /*    mediaActive: {
     type: Boolean,
     label: 'Active Media',
     defaultValue: true
     },

     userName: {
     type: String,

     autoform: {
     type: 'hidden',
     label: false
     }
     },*/

    division: {
        type: String,
        label: 'Division',

        // allowedValues: MNM.Collections.adminValueSet.find({type:"Division"}).map(function(obj) {return obj.value}),
        autoform: {
            afFieldInput: {
                type: 'select',
                options: function () {
                    return MNM.Collections.adminValueSet.find({type: "Division"}).map(function (obj) {
                        return {label: obj.value, value: obj.value}
                    })
                }
            }
        }

    },

    coe: {
        type: String,
        label: 'Center Of Excellence',
        // max: 5,
        //allowedValues: MNM.Collections.adminValueSet.find({type:"COE"}).map(function(obj) {return obj.value}),
        autoform: {
            afFieldInput: {
                type: 'select',
                //multiple: true,
                options: function () {
                    var division = AutoForm.getFieldValue('division');
                    return MNM.Collections.adminValueSet.find({type: "COE", parentValue: division}).map(function (obj) {
                        return {label: obj.value, value: obj.value}
                    })
                }
            }
        }
    },
    teamSize: {
        type: Number,
        label: 'Team Size',

        optional: false
    },
    period: {
        type: String,
        label: 'Period',
        max: 10,
        //  allowedValues: MNM.Collections.adminValueSet.find({type:"Period"}).map(function(obj) {return obj.value}),
        autoform: {
            afFieldInput: {
                type: 'select',
                //multiple: true,
                options: function () {
                    return MNM.Collections.adminValueSet.find({type: "Period"}).map(function (obj) {
                        return {label: obj.value, value: obj.value}
                    })
                }
            }
        }
    },
    Quarter: {
        type: String,
        label: 'Quarter',
        max: 10,
        // allowedValues:MNM.Collections.adminValueSet.find({type:"Quarter"}).map(function(obj) {return obj.value}),
        autoform: {
            afFieldInput: {
                type: 'select',
                //multiple: true,
                options: function () {
                    return MNM.Collections.adminValueSet.find({type: "Quarter"}).map(function (obj) {
                        return {label: obj.value, value: obj.value}
                    })
                }
            }
        }
    },
    KMIInputsInfo: {
        type: MNM.Schemas.KmiCoeInputSchema,
        label: "KMI Inputs",
        // optional: true,

    },
})

MNM.Collections.Kmi.attachSchema(MNM.Schemas.KmiSchema);
MNM.Collections.KmiGroups.attachSchema(MNM.Schemas.KmiParameterGroup);

MNM.Collections.KmiCOEInfo.attachSchema(MNM.Schemas.KmiCoeSetupSchema);
MNM.Collections.adminValueSet.attachSchema(MNM.Schemas.adminValueSet);


MNM.Collections.Kmi.helpers({
    getScoreOfEachGroup()
{
    var kmiGroupsResult = this.KMIInputsInfo.KMIGroupsInfo.map(function (thisObj) {
        var paramResult = thisObj.groupParameters.map(function (paramObj) {
            var measureResult = paramObj.measure.map(function (measure) {
                var index = 0;
                if (measure.typeOfScore == "Ratio") {
                    if ( measure.minimumExpectedQty ==0 )
                    {
                        index = 0;
                    }
                    else
                    {
                        index = measure.actualQty / measure.minimumExpectedQty
                    }
                }
                else if (measure.typeOfScore == "Difference") {
                    index = measure.actualQty - measure.minimumExpectedQty;
                    if (index < 0) index = -1;
                    if (index > 0) index = 1;
                    console.log(index)
                }
                //console.log(index)
                var score = measure.scoringCriteria.find(function (criteria) {
                    //  console.log(criteria)
                    if (criteria.endRange > 1) criteria.endRange=20;
                    if ((this >= criteria.startRange||this ==    0) && (this < criteria.endRange|| this == 0))
                        return true;
                }, index)
                var retObj = new Object();
                retObj.measureDescription = measure.measureDescription;
                retObj.measureScore = score.score;
                retObj.weightAge = measure.measureWeightage;

                return retObj;

            })
            var paramScore = 0;
            for (i = 0; i < measureResult.length; i++) {
                paramScore = paramScore + (measureResult[i].measureScore * measureResult[i].weightAge) / 100;
            }
            var retObj = new Object();
            retObj.paramName = paramObj.parameterName;
            retObj.paramScore = paramScore;
            retObj.parameterWeightage = paramObj.parameterWeightage
            retObj.measureResult = measureResult;
            // console.log(retObj);
            return retObj;

        })
        var paramGroupScore = 0;
        for (i = 0; i < paramResult.length; i++) {
            paramGroupScore = paramGroupScore + (paramResult[i].paramScore * paramResult[i].parameterWeightage) / 100;
        }
        var retObj = new Object();
        retObj.paramGrpName = thisObj.parameterGroupName;
        retObj.paramGrpScore = paramGroupScore;
        retObj.paramGrpWeightage = thisObj.groupWeightage;
        retObj.paramResult = paramResult;
        return retObj;
    })
    var totalScore = 0;
    for (i = 0; i < kmiGroupsResult.length; i++) {
        totalScore = totalScore + (kmiGroupsResult[i].paramGrpScore * kmiGroupsResult[i].paramGrpWeightage) / 100;
    }
    var retObj = new Object();
    retObj.totalScore = totalScore;
    retObj.kmiGroupsResult = kmiGroupsResult;
    return retObj;
}
,
})
;

AdminConfig = {
    name: 'MnM KMI Admin',
    collections: {
        "MNM.Collections.Kmi": {},
        "MNM.Collections.KmiGroups": {},

        "MNM.Collections.KmiCOEInfo": {},
        "MNM.Collections.adminValueSet": {}
    }
};