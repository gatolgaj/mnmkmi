/**
 * Created by shyam on 16/03/17.
 */
Meteor.startup(function () {

    var coeAD = ["Body In White", "Brake", "Drive Away chassis", "Electrical & Electronics", "Seats", "Validation", "Vehicle Integration", "Trims", "Vehicle safety and restraints", "Engine support systems", "Homologation", "Thermal Engineering -Cooling", "Thermal Engineering -HVAC", "Transmission", "Build", "Clutch", "CAE -Durability", "CAE -Vehicle Dynamics", "CAE -Occupant safety systems", "CAE -Crash", "Engine"]
    var coeFD = [ "Material Technology", "Noise Vibration & Harshness", "System Evaluation Lab", "Fatigue Lab."]
    var coeATS = ["Body Engineering Aggregates", "Building & Validation", "Advance Engineering", "Transmission", "Vehicle Integration", "Hydraulics", "CAE", "Engine"]

    var Division = ["AD", "FD", "ATS"]

    var Quarter = ["Q1", "Q2", "Q3", "Q4", "H1", "H2"]

    var Period = ["FY14", "FY15", "FY16", "FY17", "FY18"]
    if (MNM.Collections.adminValueSet.find().count() == 0) {
        coeAD.map(function (c) {
            MNM.Collections.adminValueSet.insert({type: "COE", value: c,parentValue:"AD"})
        })
        coeFD.map(function (c) {
            MNM.Collections.adminValueSet.insert({type: "COE", value: c,parentValue:"FD"})
        })
        coeATS.map(function (c) {
            MNM.Collections.adminValueSet.insert({type: "COE", value: c,parentValue:"ATS"})
        })
        Division.map(function (c) {
            MNM.Collections.adminValueSet.insert({type: "Division", value: c})
        })
        Quarter.map(function (c) {
            MNM.Collections.adminValueSet.insert({type: "Quarter", value: c})

        })
        Period.map(function (c) {
            MNM.Collections.adminValueSet.insert({type: "Period", value: c})

        })
    }

})